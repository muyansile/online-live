const baseUrl = 'http://127.0.0.1:8088'
const token = localStorage.getItem("token")

const	rerequest_get = (url) => {
	 return	uni.request({
			url: baseUrl + url,
			method: "GET",
			header: {
				token
			}
		})
	}

const	request_get_param = (url, param) => {
	return	uni.request({
			url: baseUrl + url,
			data: param,
			method: "GET",
			header: {
				token
			}
		})
	}

const	request_post = (url, param) => {
	return	uni.request({
			url: baseUrl + url,
			data: param,
			method: "POST",
			header: {
				token
			}
		})
	}
	
export default request_post
